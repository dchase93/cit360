import java.io.*;
import java.util.*;

public class Account {
    public String accountNumber, accountType, givenName, familyName;

    public Account(String accountNumber, String accountType, String givenName, String familyName) {
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.givenName = givenName;
        this.familyName = familyName;
    }


    public static void main(String[] args) {
        File myFile = new File("rawData.csv");
        File newFile = new File("newAccountInfo.csv");

        TreeMap<Integer, String> tmap = new TreeMap<Integer, String>();

        try {
            Account[] accounts = new Account[280000];
            String[] read = readText(myFile);
            int counter = 0;
            for (int i=0; i<read.length; i++) {
                String splitLines[] = read[i].split(",");
                accounts[i] = new Account(splitLines[0], splitLines[1], splitLines[2],splitLines[3]);

                if (i>1 && !accounts[i].accountNumber.equals(accounts[i - 1].accountNumber)) {
                    counter++;
                    tmap.put(counter, (accounts[i].accountNumber + "," + accounts[i].accountType + "," + accounts[i].givenName + "," + accounts[i].familyName));
                }
            }

            writeText(newFile, tmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static String[] readText(File myFile) throws IOException {
        ArrayList<String> fullText = new ArrayList<>();

        BufferedReader br = null;
        FileReader fr = new FileReader(myFile);
        try {
            br = new BufferedReader(fr);

            String line;
            while ((line = br.readLine()) != null) {
                fullText.add(line);
            }
        } finally {
            if (br != null) {
                br.close();
            } else {
                fr.close();
            }
        }

        String[] textArray = new String[fullText.size()];
        return fullText.toArray(textArray);
    }


    public static void writeText(File newFile, TreeMap tmap) throws IOException {
        PrintWriter pw = null;
        BufferedWriter bw = null;
        FileWriter fw = new FileWriter(newFile);
        try {
            bw = new BufferedWriter(fw);
            pw = new PrintWriter(bw);

            Set set = tmap.entrySet();
            Iterator iterator = set.iterator();
            while(iterator.hasNext()) {
                Map.Entry mapEntry = (Map.Entry)iterator.next();
                pw.println(mapEntry.getValue());
                System.out.println(mapEntry.getValue());
            }
        } finally {
            if (pw != null) {
                pw.close();
            } else if (bw != null) {
                bw.close();
            } else {
                fw.close();
            }
        }
    }

}
