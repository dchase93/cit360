public class Kennel {
    private static DogBean[] buildDogs() {
        DogBean[] newDogs = new DogBean[] {
                new DogBean(4, "Yellow", 34.2, "Buddy", "Golden Retreiver"),
                new DogBean(4, "Black", 29.8, "Boss", "Lab"),
                new DogBean(4, "Brown", 24.0, "Scruff", "Mutt")
        };
        return newDogs;
    }

    private static void displayDogs(DogBean[] allDogs) {
        for (int i=0; i<allDogs.length; i++) {
            System.out.println(allDogs[i].dogToString());
        }
    }

    public static void main(String[] args) {
        DogBean[] allDogs = buildDogs();
        displayDogs(allDogs);
    }
}
