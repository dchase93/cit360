public class DogBean extends MammalBean {
    private String name, breed;

    public DogBean(int legCount, String color, double height, String name, String breed) {
        super(legCount, color, height);
        this.name = name;
        this.breed = breed;
    }

    public String dogToString() {
        return "Leg Count = " + this.getLegCount()
                + "\nColor = " + this.getColor()
                + "\nHeight = " + this.getHeight()
                + "\nName = " + this.name
                + "\nBreed = " + this.breed + "\n";
    }

    public String getName() {return this.name;}
    public void setName(String name) {this.name = name;}

    public String getBreed() {return this.breed;}
    public void setBreed(String breed) {this.breed = breed;}

}
