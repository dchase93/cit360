import org.junit.Test;
import static org.junit.Assert.*;

public class MammalBeanTest {
    MammalBean[] mammalTestArray = {
            new MammalBean(2, "Black", 61.25),
            new MammalBean(4, "Yellow", 18.8),
            new MammalBean(4, "White", 42.3)
    };


    @Test
    public void mammalArrayLength() {
        assertEquals(3, mammalTestArray.length);
    }


    @Test
    public void getLegCount() {
        MammalBean testLegCount = new MammalBean(4, "White", 34.2);
        assertEquals(4, testLegCount.getLegCount());
    }


    @Test
    public void getColor() {
        MammalBean testColor = new MammalBean(3, "Brown", 29.5);
        assertEquals("Brown", testColor.getColor());

        assertEquals("Yellow", mammalTestArray[1].getColor());
    }


    @Test
    public void getHeight() {
        MammalBean testHeight = new MammalBean(4, "Black", 31.1);
        double delta = 0.0;
        assertEquals(31.1, testHeight.getHeight(), delta);
    }

}