import org.junit.Test;
import java.util.HashMap;
import static org.junit.Assert.*;

public class DogBeanTest {
    HashMap<String, DogBean> dogTestMap = new HashMap<>();


    @Test
    public void getName() {
        DogBean testDogName = new DogBean(4, "Yellow", 28.2, "Chief", "Lab");
        assertEquals("Chief", testDogName.getName());

        dogTestMap.put("Joey", new DogBean(4, "Golden", 34.8, "Joey", "Golden Retriever"));
        assertEquals(true, dogTestMap.get("Joey") != null);
    }


    @Test
    public void getBreed() {
        DogBean testDogBreed = new DogBean(3, "Black", 33.9, "Bob", "Mutt");
        assertEquals("Mutt", testDogBreed.getBreed());

        dogTestMap.put("Buddy", new DogBean(4, "Black", 19.6, "Buddy", "Lab"));
        dogTestMap.remove("Buddy");
        assertEquals(true, dogTestMap.get("Buddy") == null);
    }

}